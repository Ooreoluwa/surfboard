class Topic:
    def __init__(self, title, timeEstimate, textDescription):
        """
        Creates a topic instance
        -Has a title, time estimate and text description
        :return: None
        """
        self.title = title
        self.timeEstimate = timeEstimate
        self.textDescription = textDescription


class Agenda:
    def __init__(self, name):
        """
        Creates an agenda instance
        - Has a name
        :return: None
        """
        self.name = name
        self.topics = []

    def printAllTopics(self):
        """
        Prints all topics within the agenda
        - No parameters
        :return: None
        """
        if self.topics != []:
            print("Agenda: " + self.name)
            for index, topic in enumerate(self.topics):
                print(str(index + 1) + ". " + topic.title + "; Time: " + str(topic.timeEstimate) + " mins, Description: " + topic.textDescription)
        else:
            print("There are no available topics on the agenda at the moment. Add one to the agenda.")
    
    def getNewestTopic(self):
        """
        Gets the newest topic added to the agenda
        - No parameters
        :return: None
        """
        if self.topics:
            print("Newest topic addition is: " + self.topics[-1].title)
        else:
            print("There are no topics available at the moment. Add one to the agenda.")


class User:
    def __init__(self, name, isPresenter = False):
        """
        Creates an User instance
        - Has a name and presenter status
        :return: None
        """
        self.name = name
        self.messages = {}
        self.currentTopic = None
        self.isPresenter = isPresenter
        if self.isPresenter:
            print("Hello " + self.name + "! You are the presenter.")
        else:
            print("Hello " + self.name + "!")

    def addTopic(self, agenda, topicTitle, timeEstimate, textDescription):
        """
        Adds a topic to the agenda
        - Takes the agenda instance name, topic title, time estimate and text description
        :return: None
        """
        if self.isPresenter:
            newTopic = Topic(topicTitle, timeEstimate, textDescription)
            agenda.topics.append(newTopic)
            print(self.name + " added: " + topicTitle)
        else:
            print(self.name + ", you are not the presenter. Ask the presenter to add a topic.")
    
    def editTopic(self, agenda, topicIndex, title = "", timeEstimate = 0, textDescription = ""):
        """
        Edits a topic within the agenda
        - Takes the agenda instance name, topic title, time estimate and or description
        :return: None
        """
        if self.isPresenter:
            if title:
                agenda.topics[topicIndex - 1].title = title

            if timeEstimate:
                agenda.topics[topicIndex - 1].timeEstimate = timeEstimate

            if textDescription:
                agenda.topics[topicIndex - 1].textDescription = textDescription
        else:
            print(self.name + ", you are not the presenter. Ask the presenter to edit a topic.")


    def deleteTopic(self, agenda, topicIndex):
        """
        Deletes a topic within the agenda
        - Takes the agenda instance name, and the topic index
        :return: None
        """
        if self.isPresenter:
            deletedTopic = agenda.topics.pop(topicIndex - 1)
            print(self.name + " deleted the topic '" + deletedTopic.title + "' from the agenda.")
        else:
            print(self.name + ", you are not the presenter. Ask the presenter to delete a topic.")

    def changeCurrentTopic(self, agenda, topicIndex):
        """
        Changes the current topic the user is looking at
        - Takes the agenda instance name, and the topic index
        :return: None
        """
        self.currentTopic = agenda.topics[topicIndex - 1]

    def printCurrentTopic(self):
        """
        Prints the current topic title
        - Takes no parameters
        :return: None
        """
        if self.currentTopic:
            print("Current topic for " + str(self.name) + " is: " + str(self.currentTopic.title))
        else:
            print(self.name + " has no current topic at the moment. Chose one.")

    def sendChat(self, otherUser, message):
        """
        Sends a chat to another user
        - Takes the name of the other user instance, and the message to send
        :return: None
        """
        if otherUser in self.messages.keys():
            #adds the messages to both the sender and receiver's list of messages
            currentMessages = self.messages[otherUser]
            currentMessages.append(self.name + ": " + message)
            self.messages[otherUser] = currentMessages
            currentMessages = otherUser.messages[self]
            currentMessages.append(self.name + ": " + message)
            otherUser.messages[self] = currentMessages
            print("Hey " + self.name + ", " + otherUser.name + " sent you a message!")
        else:
            self.messages[otherUser] = [self.name + ": " + message]
            otherUser.messages[self] = [self.name + ": " + message]
            print("Hey " + self.name + ", " + otherUser.name + " sent you a message!")
        
    def printAllMessages(self):
        """
        Prints all messages for the user
        - Takes no parameters
        :return: None
        """
        print("All messages for " + self.name)
        if self.messages:
            users = list(self.messages.keys())
            i = 0
            for messages in self.messages.values():
                print("Chat with " +  users[i].name + ": ")
                for message in messages:
                    print(message)
                i += 1
                print("\n")
        else:
            print(self.name + " does not have any messages at this current time.")

    def printSpecificChat(self, otherUser):
        """
        Prints the user's chat with another user
        - Takes no parameters
        :return: None
        """
        if self.messages:
            if otherUser in self.messages:
                print("Messages between " + self.name + " and " + otherUser.name)
                for message in self.messages[otherUser]:
                    print(message)
            else:
                print(self.name + " does not have any message from " + otherUser.name + " at the current time.")
        else:
            print(self.name + " does not have any messages at this current time.")


#Tests
agenda1 = Agenda("Food")
agenda1.printAllTopics()
print("\n")

user1 = User("Oore", True)
user1.addTopic(agenda1, "Biscuits and Gravy", 30, "A short discussion about sweet biscuits and gravy.")
user1.addTopic(agenda1, "Drinks", 45, "Soda vs Water.")
user1.addTopic(agenda1, "Candy", 60, "What is your favorite candy?")
user1.addTopic(agenda1, "Technology", 20 , "Apple vs Android.")
print("\n")

agenda1.printAllTopics()
print("\n")

agenda1.getNewestTopic()
print("\n")

user1.deleteTopic(agenda1, 4)
print("\n")

agenda1.printAllTopics()
print("\n")

user1.changeCurrentTopic(agenda1, 3)
user1.printCurrentTopic()
print("\n")

user1.printAllMessages()
print("\n")

user2 = User("Lily")
print("\n")

user2.deleteTopic(agenda1, 2)
print("\n")

user2.sendChat(user1, "Can you please delete the topic 'Candy'?")
user1.sendChat(user2, "No can do. That is my favorite topic on the agenda.")
user2.sendChat(user1, "Okay, no worries. Thank you!")
print("\n")

user1.printSpecificChat(user2)
print("\n")


user3 = User("Daniel")
print("\n")

user3.sendChat(user2, "Do you also want to take 'Candy' off the agenda")
user2.sendChat(user3, "Yes I definitely do")
print("\n")

user3.sendChat(user1, "We all want 'Candy' off the list. Please take it off")
user1.sendChat(user3, "Fine!")
print("\n")

user1.deleteTopic(agenda1, 3)
print("\n")

agenda1.printAllTopics()
print("\n")

user1.printAllMessages()
print("\n")

user2.printAllMessages()
print("\n")

user3.printAllMessages()


