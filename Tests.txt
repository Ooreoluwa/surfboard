#Tests
agenda1 = Agenda("Food")
agenda1.printAllTopics()
print("\n")

user1 = User("Oore", True)
user1.addTopic(agenda1, "Biscuits and Gravy", 30, "A short discussion about sweet biscuits and gravy.")
user1.addTopic(agenda1, "Drinks", 45, "Soda vs Water.")
user1.addTopic(agenda1, "Candy", 60, "What is your favorite candy?")
user1.addTopic(agenda1, "Technology", 20 , "Apple vs Android.")
print("\n")

agenda1.printAllTopics()
print("\n")

agenda1.getNewestTopic()
print("\n")

user1.deleteTopic(agenda1, 4)
print("\n")

agenda1.printAllTopics()
print("\n")

user1.changeCurrentTopic(agenda1, 3)
user1.printCurrentTopic()
print("\n")

user1.printAllMessages()
print("\n")

user2 = User("Lily")
print("\n")

user2.deleteTopic(agenda1, 2)
print("\n")

user2.sendChat(user1, "Can you please delete the topic 'Candy'?")
user1.sendChat(user2, "No can do. That is my favorite topic on the agenda.")
user2.sendChat(user1, "Okay, no worries. Thank you!")
print("\n")

user1.printSpecificChat(user2)
print("\n")


user3 = User("Daniel")
print("\n")

user3.sendChat(user2, "Do you also want to take 'Candy' off the agenda")
user2.sendChat(user3, "Yes I definitely do")
print("\n")

user3.sendChat(user1, "We all want 'Candy' off the list. Please take it off")
user1.sendChat(user3, "Fine!")
print("\n")

user1.deleteTopic(agenda1, 3)
print("\n")

agenda1.printAllTopics()
print("\n")

user1.printAllMessages()
print("\n")

user2.printAllMessages()
print("\n")

user3.printAllMessages()


#Results
There are no available topics on the agenda at the moment. Add one to the agenda.


Hello Oore! You are the presenter.
Oore added: Biscuits and Gravy
Oore added: Drinks
Oore added: Candy
Oore added: Technology


Agenda: Food
1. Biscuits and Gravy; Time: 30 mins, Description: A short discussion about sweet biscuits and gravy.
2. Drinks; Time: 45 mins, Description: Soda vs Water.
3. Candy; Time: 60 mins, Description: What is your favorite candy?
4. Technology; Time: 20 mins, Description: Apple vs Android.


Newest topic addition is: Technology


Oore deleted the topic 'Technology' from the agenda.


Agenda: Food
1. Biscuits and Gravy; Time: 30 mins, Description: A short discussion about sweet biscuits and gravy.
2. Drinks; Time: 45 mins, Description: Soda vs Water.
3. Candy; Time: 60 mins, Description: What is your favorite candy?


Current topic for Oore is: Candy


All messages for Oore
Oore does not have any messages at this current time.


Hello Lily!


Lily, you are not the presenter. Ask the presenter to delete a topic.


Hey Lily, Oore sent you a message!
Hey Oore, Lily sent you a message!
Hey Lily, Oore sent you a message!


Messages between Oore and Lily
Lily: Can you please delete the topic 'Candy'?
Oore: No can do. That is my favorite topic on the agenda.
Lily: Okay, no worries. Thank you!


Hello Daniel!


Hey Daniel, Lily sent you a message!
Hey Lily, Daniel sent you a message!


Hey Daniel, Oore sent you a message!
Hey Oore, Daniel sent you a message!


Oore deleted the topic 'Candy' from the agenda.


Agenda: Food
1. Biscuits and Gravy; Time: 30 mins, Description: A short discussion about sweet biscuits and gravy.
2. Drinks; Time: 45 mins, Description: Soda vs Water.


All messages for Oore
Chat with Daniel: 
Daniel: We all want 'Candy' off the list. Please take it off
Oore: Fine!


Chat with Lily: 
Lily: Can you please delete the topic 'Candy'?
Oore: No can do. That is my favorite topic on the agenda.
Lily: Okay, no worries. Thank you!




All messages for Lily
Chat with Daniel: 
Daniel: Do you also want to take 'Candy' off the agenda
Lily: Yes I definitely do


Chat with Oore: 
Lily: Can you please delete the topic 'Candy'?
Oore: No can do. That is my favorite topic on the agenda.
Lily: Okay, no worries. Thank you!




All messages for Daniel
Chat with Oore: 
Daniel: We all want 'Candy' off the list. Please take it off
Oore: Fine!


Chat with Lily: 
Daniel: Do you also want to take 'Candy' off the agenda
Lily: Yes I definitely do
