# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Surfboard Product ###
  
* A product for users to run investor meetings in. The product meets the following product requirements:

* An agenda
    * A list of meeting topics
        * Each topic contains a title, time estimate, and a text description
    * Users should be able to navigate between topics
    * The presenter should be able to add, delete and edit topics
* Users can communicate with each other in some way (chat, audio, etc.)

### Dependencies? ###
* Python 3.8

### how to run tests ###
* To run tests, copy the code in the Tests word file into your terminal

### Things to Note
* Indexing starts from 1 to allow user-friendliness 
